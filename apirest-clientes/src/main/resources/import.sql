insert into regiones (nombre) values ('Europa');
insert into regiones (nombre) values ('Asia');
insert into regiones (nombre) values ('Africa');
insert into regiones (nombre) values ('Oceania');
insert into regiones (nombre) values ('Antartida');
insert into regiones (nombre) values ('Sudamerica');
insert into regiones (nombre) values ('Centroamerica');
insert into regiones (nombre) values ('Norteamerica');

insert into clientes (nombre,apellido,email,telefono,create_at,id_region) values('Jose','Diaz','jd@email.com',6321545,'2022-10-24',1);
insert into clientes (nombre,apellido,email,telefono,create_at,id_region) values('Carlos','Gomez','cg@email.com',6321545,'2022-10-24',2);
insert into clientes (nombre,apellido,email,telefono,create_at,id_region) values('Maria','Lopez','ml@email.com',6321545,'2022-10-24',3);
insert into clientes (nombre,apellido,email,telefono,create_at,id_region) values('Dina','Orellana','do@email.com',6321545,'2022-10-24',4);
insert into clientes (nombre,apellido,email,telefono,create_at,id_region) values('Mirna','Ramos','mr@email.com',6321545,'2022-10-24',5);
insert into clientes (nombre,apellido,email,telefono,create_at,id_region) values('Marco','Mojica','mm@email.com',6321545,'2022-10-24',6);
insert into clientes (nombre,apellido,email,telefono,create_at,id_region) values('Juan','Perez','jp@email.com',6321545,'2022-10-24',7);
insert into clientes (nombre,apellido,email,telefono,create_at,id_region) values('Pedro','Narvaez','pn@email.com',6321545,'2022-10-24',8);
insert into clientes (nombre,apellido,email,telefono,create_at,id_region) values('Ramon','Iglesias','ri@email.com',6321545,'2022-10-24',1);
insert into clientes (nombre,apellido,email,telefono,create_at,id_region) values('Miguel','Gonzalez','mg@email.com',6321545,'2022-10-24',1);

insert into usuarios (username,password,enabled) values('rolando','$2a$10$iZrJgh66j.L/W5cGM4023ukmu9UfPA8pLVtPmCB.fmhZWPKzyDnEq',1);
insert into usuarios (username,password,enabled) values('admin','$2a$10$1Ormv8ADiBJaO6oCRwTHTePfJ9nTx3fk6c4WbtAvVB3GWU90JvsaS',1);

insert into roles (nombre) values('ROLE_USER');
insert into roles (nombre) values('ROLE_ADMIN');

insert into usuarios_roles (usuario_id,role_id) values(1,1);
insert into usuarios_roles (usuario_id,role_id) values(2,2);