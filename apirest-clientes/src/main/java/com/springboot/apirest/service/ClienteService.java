package com.springboot.apirest.service;

import java.util.List;

import com.springboot.apirest.entity.Cliente;
import com.springboot.apirest.entity.Region;

public interface ClienteService {

	//metodo para buscar todos los clientes
	public List<Cliente> findClients();
	//metodo para buscar cliente por id
	public Cliente findClientById(Long id);
	//metodo para registrar un cliente
	public Cliente saveClient(Cliente cliente);
	//metodo para borrar un cliente
	public Cliente delete(Long id);
	
	//metodo para buscar todas las regiones
	public List<Region> findRegions();
	//metodo para buscar por nombre de cliente
	public Cliente buscarPorEmail(String email);
}
