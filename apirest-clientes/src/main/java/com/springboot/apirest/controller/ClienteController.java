package com.springboot.apirest.controller;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.management.RuntimeErrorException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.springboot.apirest.entity.Cliente;
import com.springboot.apirest.entity.Region;
import com.springboot.apirest.service.ClienteService;

@RestController
@RequestMapping("/api")
public class ClienteController {

	@Autowired
	private ClienteService clienteService;
	
	//@RequestMapping(value="clientes",method=RequestMethod.GET)
	//@RequestMapping("clientes")
	@GetMapping("/clientes")
	public List<Cliente> index(){
		return clienteService.findClients();
	}
	
	/*@GetMapping("/clientes/{id}")
	public Cliente show(@PathVariable Long id) {
		return clienteService.findClientById(id);
	}*/
	
	@GetMapping("/clientes/{id}")
	public ResponseEntity<?> show(@PathVariable Long id){
		Cliente cliente = null;
		Map<String,Object> response = new HashMap<>();
		
		try {
			cliente = clienteService.findClientById(id);
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar consulta a la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		if(cliente == null) {
			response.put("mensaje", "El cliente con ID:"+id.toString()+" no existe en la base de datos");
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<Cliente>(cliente,HttpStatus.OK);
		
	}
	
	
	/*@PostMapping("/clientes")
	@ResponseStatus(HttpStatus.CREATED)
	public Cliente create(@RequestBody Cliente cliente) {
		return clienteService.saveClient(cliente);
	}*/
	@PostMapping("/clientes")
	public ResponseEntity<?> create(@RequestBody Cliente cliente) {
		Cliente clienteNew = null;
		Map<String,Object> response = new HashMap<>();
		
		try {
			clienteNew = clienteService.saveClient(cliente);
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar inserción a la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("mensaje", "El cliente ha sido creado con éxito!");
		response.put("cliente",clienteNew);
		
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED);
	}
	
	
	/*@PutMapping("/clientes/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public Cliente update(@RequestBody Cliente cliente,@PathVariable Long id) {
		
		Cliente clienteUpdate = clienteService.findClientById(id);
		
		clienteUpdate.setNombre(cliente.getNombre());
		clienteUpdate.setApellido(cliente.getApellido());
		clienteUpdate.setEmail(cliente.getEmail());
		clienteUpdate.setTelefono(cliente.getTelefono());
		
		return clienteService.saveClient(clienteUpdate);
	}*/
	@PutMapping("/clientes/{id}")
	public ResponseEntity<?> update(@RequestBody Cliente cliente,@PathVariable Long id) {
		Cliente clienteUpdate = clienteService.findClientById(id);
		Map<String,Object> response = new HashMap<>();
		
		if(clienteUpdate == null) {
			response.put("mensaje", "Error: no se puede editar, el cliente con ID: "+id.toString()+" no existe en la base de datos");
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.NOT_FOUND);
		}
		
		try {
			
			clienteUpdate.setNombre(cliente.getNombre());
			clienteUpdate.setApellido(cliente.getApellido());
			clienteUpdate.setEmail(cliente.getEmail());
			clienteUpdate.setTelefono(cliente.getTelefono());
			clienteUpdate.setRegion(cliente.getRegion());
			clienteService.saveClient(clienteUpdate);
			
			
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar actualiziación a la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("mensaje", "El cliente ha sido actualizado con éxito!");
		response.put("cliente",clienteUpdate);
		
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED);
	}
	
	
	/*@DeleteMapping("/clientes/{id}")
	@ResponseStatus(HttpStatus.OK)
	public Cliente delete(@PathVariable Long id) {
		return clienteService.delete(id);
	}*/
	
	@DeleteMapping("/clientes/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {
		Cliente clienteDelete = clienteService.findClientById(id);
		Map<String,Object> response = new HashMap<>();
		
		if(clienteDelete == null) {
			response.put("mensaje", "Error: no se puede eliminar, el cliente con ID: "+id.toString()+" no existe en la base de datos");
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.NOT_FOUND);
		}
		
		
		try {
			
			clienteService.delete(id);
			
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar actualiziación a la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		String nombreImagenAnterior = clienteDelete.getImagen();
		
		if( nombreImagenAnterior != null && nombreImagenAnterior.length() > 0) {
			Path rutaImagenAnterior=Paths.get("uploads").resolve(nombreImagenAnterior).toAbsolutePath();
			File archivoImagenAnterior = rutaImagenAnterior.toFile(); 
			
			if( archivoImagenAnterior.exists() && archivoImagenAnterior.canRead() ) {
				archivoImagenAnterior.delete();
			}
		}
		
		response.put("mensaje", "El cliente ha sido eliminado con éxito!");
		response.put("cliente",clienteDelete);
		
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED);
	}
	
	
	//metodo http para registrar imagen
	@PostMapping("/clientes/upload")
	public ResponseEntity<?> upload(@RequestParam("archivo") MultipartFile archivo,@RequestParam("id") Long id){
		Map<String,Object> response= new HashMap<>();
		Cliente cliente = clienteService.findClientById(id);
	  
		if(!archivo.isEmpty()) {
			//String nombreArchivo= archivo.getOriginalFilename();
			String nombreArchivo = UUID.randomUUID().toString()+"_"+archivo.getOriginalFilename().replace(" ","");
			Path rutaArchivo = Paths.get("uploads").resolve(nombreArchivo).toAbsolutePath();
		
			try {				
				Files.copy(archivo.getInputStream() , rutaArchivo);
			} catch (IOException e) {
				response.put("mensaje","Error al subir la imagen del cliente");
				response.put("error",e.getMessage().concat(": ").concat(e.getCause().getMessage()));
				return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
			String nombreImagenAnterior = cliente.getImagen();
			
			if( nombreImagenAnterior != null && nombreImagenAnterior.length() > 0) {
				Path rutaImagenAnterior=Paths.get("uploads").resolve(nombreImagenAnterior).toAbsolutePath();
				File archivoImagenAnterior = rutaImagenAnterior.toFile(); 
				
				if( archivoImagenAnterior.exists() && archivoImagenAnterior.canRead() ) {
					archivoImagenAnterior.delete();
				}
			}
			
			
			
			cliente.setImagen(nombreArchivo);
			clienteService.saveClient(cliente);
			
			response.put("cliente", cliente);
			response.put("mensaje", "Has subido correctamente la imagen: "+nombreArchivo);
		}
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED);
	}
	
	
	
	//metodo para acceder a las imagenes
	@GetMapping("/clientes/upload/img/{nombreImagen:.+}")
	public ResponseEntity<Resource> showImage(@PathVariable String nombreImagen){
		
		Path rutaArchivo= Paths.get("uploads").resolve(nombreImagen).toAbsolutePath();
		
		Resource recurso = null;
		
		try {
			
			recurso = new UrlResource(rutaArchivo.toUri());
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		
		if(!recurso.exists() && !recurso.isReadable()) {
			throw new RuntimeException("Error no se puede mostrar la imagen "+nombreImagen);
		}
		
		HttpHeaders cabecera = new HttpHeaders();
		cabecera.add(HttpHeaders.CONTENT_DISPOSITION, "attechment;filename=\""+recurso.getFilename()+"\"");
		return new ResponseEntity<Resource>(recurso,cabecera,HttpStatus.OK);
		
	}
	
	@GetMapping("/clientes/regiones")
	public List<Region> listRegions(){
		return clienteService.findRegions();
	}
	
	@GetMapping("/clientes/email/{email}")
	public Cliente searchClientForEmail(@PathVariable String email) {
		return clienteService.buscarPorEmail(email);
	}
	
}
