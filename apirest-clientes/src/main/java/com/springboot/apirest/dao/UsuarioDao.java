package com.springboot.apirest.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.springboot.apirest.entity.Usuario;


@Repository
public interface UsuarioDao extends CrudRepository<Usuario, Long>{

	//metodo para buscar por username
	public Usuario findByUsername(String username);
	
	//otra forma de declarar un metodo que busque por username
	@Query("select u from Usuario u where u.username=?1")
	public Usuario findByUsername2(String username);
}
